import axios from 'axios'
import React, { Component } from 'react'
import { CountryCard } from '../countrycard/country-card'
import './dashboard.css'
import logo from '../../assets/images/logo.png';

export class Dashboard extends Component {
    constructor(props) {
        super(props)
        this.state = {
            countries: [],
            allCountries: []
        }
    }

    componentDidMount() {
        this.getAllCountries()
        console.log('props', this.props)
    }
    getAllCountries = () => {
        axios.get('https://restcountries.eu/rest/v2/all').then(res => {
            this.setState({
                allCountries: res.data,
                countries: res.data
            })
            // this.props?.getCountriesAPI(res.data)
        })
    }

    searchHandler = (event) => {
        let searchVal = event.target.value.toLowerCase();
        var searchData = [];
        this.state.allCountries.map((country) => {
            if (country.name.toLowerCase().indexOf(searchVal) !== -1 || searchVal === '')
                searchData.push(country);
            return country;
        });
        this.setState({ countries: searchData });
    }

    render() {
        return (
            <React.Fragment>
                <div className="container">
                    <h2 className="my-2">List of all countries</h2>
                    <div className="main-container">
                        <div className="search-container">
                            <img src={logo} className="logo" />
                            <input type="text" placeholder="Search Countries here..." className="searchInput" onChange={this.searchHandler} />
                        </div>
                        <div className="row countries-container">
                            {this.state?.countries?.map(country => {
                                return (
                                    <div className="col-4" key={country}>
                                        <CountryCard country={country} />
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}
export default Dashboard;