import React, { Component } from 'react'
import './countryCard.css'

export class CountryCard extends Component {

    render() {
        return (
            <>
                <div className="country-card p-2">
                    <div className="d-flex flag-div">
                        <img src={this.props.country.flag} className="country-img" />
                        <div className="ml-2">
                            <span> <b>Conuntry name </b>: {this.props.country.name} </span> <br />
                            <span> <b>Capital name</b>: {this.props.country.capital} </span> <br />
                        </div>
                    </div>
                    <div className="text-center mt-2">
                        <span>Population</span> <br/>
                        <h3>{this.props.country.population}</h3>
                    </div>
                </div>
            </>
        )
    }
}

export default CountryCard