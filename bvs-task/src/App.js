import React, { Component } from 'react'
import { Dashboard } from './feature/Dashboard/Dashboard'
import 'bootstrap/dist/css/bootstrap.css';
import { Route, Switch, HashRouter, Redirect, BrowserRouter } from 'react-router-dom';

export class App extends Component {

  render() {
    return (
      <>
        <BrowserRouter>
          <HashRouter>
            <Switch>
              <Route exact path="/">
                <Redirect to="/dashboard" />
              </Route>
              <Route exact path="/dashboard" component={Dashboard} />
            </Switch>
          </HashRouter>
        </BrowserRouter>
      </>
    )
  }
}

export default App